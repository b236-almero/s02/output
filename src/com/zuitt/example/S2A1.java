package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class S2A1 {
    public static void main(String[] args) {
        //Leap Year Checker
        Scanner yearScanner = new Scanner(System.in);
        System.out.println("Input year to be checked if a leap year.");
        int inputYear = yearScanner.nextInt();

        if((inputYear % 4 == 0 && inputYear % 100 != 0) || inputYear % 400 == 0){
            System.out.println(inputYear + " is a leap year");
        }else {
            System.out.println(inputYear + " is NOT a leap year");
        }

        //Declare and Display Array
        String[] courses = {"HTML", "CSS", "Javascript"};
        System.out.println(Arrays.toString(courses));

        //Declare and Display ArrayList
        ArrayList<String> packages = new ArrayList<>();
        packages.add("Short Course Full Time");
        packages.add("Short Course Part Time");
        packages.add("Main Package Full Time");
        packages.add("Main Package Part Time");
        System.out.println(packages);

        //Declare and Display HashMap
        HashMap<String, String> topics = new HashMap<>();
        topics.put("Front End", "HTML,CSS,JS,Bootstrap");
        topics.put("Back End", "MongoDB,Mongoose,Node,Express");
        topics.put("Full Stack", "React.js");
        System.out.println(topics);


    }
}
