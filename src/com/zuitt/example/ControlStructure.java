package com.zuitt.example;

import java.util.Scanner;

public class ControlStructure {
    public static void main(String[] args){
        //Java Operators ---------------------------------------->
        //Arithmetic + - * / %
        //Comparison > < >= <= == !=
        //Logical && || !
        //Assignment =

        //Selection Control Structure
        //If Else
        int num = 36;
        if (num % 5 == 0){
            System.out.println(num + " is divisible by 5.");
        }else {
            System.out.println(num + " is not divisible by 5.");
        }

        //Short Circuiting
        int x = 15;
        int y = 0;
        if( y != 0 && x/y == 0){
            System.out.println("Result is: " + x/y);
        }
        //Ternary Operator
        int num1 = 24;
        Boolean result = (num1 > 0) ? true : false;

        //Switch Cases
        Scanner numberScanner = new Scanner(System.in);
        System.out.println("Enter a number: ");
        int directionValue = numberScanner.nextInt();

        switch (directionValue){
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("West");
                break;
            case 4:
                System.out.println("East");
                break;
            default:
                System.out.println("Invalid");
        }
    }
}
