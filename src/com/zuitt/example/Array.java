package com.zuitt.example;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;

public class Array {
    //Java Collection ------------------------------>
    //single units of objects
    //useful for manipulating relevant pieces of data used in diff. situations

    public static void main(String[] args){
        //Arrays ------------------------------>
        //containers of values of same data types given a predefined amount of values
        //dataType[] identifier = new dataType[numOfElements];
        int[] intArray = new int[5];

        //Array Declaration Method 1:
        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 98;

        System.out.println(Arrays.toString(intArray));

        //Array Declaration Method 2:
        String[] names = {"John", "Jane", "Joe"};
        /*names[4] = "Joey";*/

        System.out.println(Arrays.toString(names));

        // Sorting Arrays
        Arrays.sort(intArray);
        System.out.println("Order of items after sort: "+ Arrays.toString(intArray));

        //Multidimensional Arrays
        //Syntax: dataType[][] identifier = new dataType[rowLength][colLength];
        String[][] classroom = new String[3][3];

        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        //Use "deepToString" for multidimensional arrays
        System.out.println(Arrays.deepToString(classroom));


        //ArrayLists - resizable arrays, wherein elements can be added or removed whenever it is needed.
        //Syntax: ArrayList<datatype> identifier = new ArrayList<dataType>();
        //Only non-primitive data types are allowed for ArrayList
        //if ArrayList of numbers, we can use Integer (use counterparts of primitive)
        ArrayList<String> students = new ArrayList<String>();

        //Add element to ArrayList
        students.add("John");
        students.add("Paul");
        System.out.println(students);

        //Access element
        //Syntax: arrayListName.get(index);
        System.out.println(students.get(1));

        //Adding element on a specific index
        //Syntax: arrayListName.add(index, value/element);
        students.add(1, "Mike");
        System.out.println(students);

        //Updating element on specific index
        //Syntax: arrayListName.set(index, element);
        students.set(1, "George");
        System.out.println(students);

        //Removing element on specific index
        //Syntax: arrayListName.remove(index);
        students.remove(1);
        System.out.println(students);

        //Remove all elements
        students.clear();
        System.out.println(students);

        //Getting arrayList size
        System.out.println(students.size());


        //Objects ----------------------->
        // most objects in Java are defined and are instantiations of Classes that contain a proper set of properties and methods.
        // There are might be use cases where is this not appropriate, or you may simply want to store a collection of data in key-value pairs
        // in Java "keys" also referred as "fields"
        // wherein the values are accessed by the fields
        // Syntax:
        // HashMap<dataTypeField, dataTypeValue> identifier = new HashMap<dataTypeField, dataTypeValue>();

        //Declaring HashMaps
        HashMap<String, String>jobPosition = new HashMap<String, String>();

        //Add element to HashMap
        //hashMapName.put(<field>,<value>);
        jobPosition.put("Student", "Brandon");
        jobPosition.put("Dreamer", "Alice");
        jobPosition.put("Student", "Brandonna"); //duplicate fields will not be replaced

        System.out.println(jobPosition);

        //Access hashMap element
        //hashMapName.get("field");
        System.out.println(jobPosition.get("Dreamer"));

        //Updating the values
        //hashMapName.replace(keyToChanged, newValue);
        jobPosition.replace("Student", "Brandon Smith");
        System.out.println(jobPosition);

        //Remove an element
        //hashMapName.remove(key/field);
        jobPosition.remove("Dreamer");
        System.out.println(jobPosition);

        //Clear all the content
        //hashMapNames.clear();
        jobPosition.clear();
        System.out.println(jobPosition);

    }
}
